require "./lib/gofish.rb"
require "test/unit"

class TestGame < Test::Unit::TestCase
  def setup
    player_names_2 = [ "Mateja", "Dominik" ]
    @game_2 = Game.new(player_names_2)
    player_names_4 = [ "Zvonko", "Ana", "Mateja", "Dominik" ]
    @game_4 = Game.new(player_names_4)
  end

  def test_init()
    assert_equal(@game_2.players.length, 2)
    assert @game_2.deck
    assert_equal(@game_4.players.length, 4)
    assert @game_4.deck
  end

  def test_valid_player()
    assert @game_2.valid_player?("Mateja")
    assert not(@game_2.valid_player?("Ana"))
  end
end
