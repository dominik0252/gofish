require "./lib/gofish.rb"
require "test/unit"

class TestDeck < Test::Unit::TestCase
  N_CARDS_IN_DECK = 52

  include Deck

  def setup
    @ace_spades  = Card.new("A","spades")
    @deck        = GameDeck.new
    @player      = Player.new("TestPlayer")
  end

  def test_card
    assert_equal(@ace_spades.rank, "A")
    assert_equal(@ace_spades.color, "spades")
  end

  def test_deck_init
    assert_equal(@deck.length, N_CARDS_IN_DECK)
    check_duplicates(@deck)
    first_five = @deck.take(5)
    @deck.shuffle!
    first_five_shuffled = @deck.take(5)
    assert_not_equal(first_five,first_five_shuffled)
  end

  def test_deal
    deck_length     = @deck.length
    player_cards_n  = @player.cards.length
    @deck.deal(@player)
    assert_equal(@deck.length, deck_length - 1)
    assert_equal(@player.cards.length, player_cards_n + 1)
  end

  private

  def check_duplicates(deck)
    assert_equal(deck.length, N_CARDS_IN_DECK)
    COLORS.each do |color|
      assert_equal( deck.select{ |card| card.color == color }.length,
                    RANKS.length)
    end
    RANKS.each do |rank|
      assert_equal( deck.select{ |card| card.rank == rank }.length,
                    COLORS.length)
    end
  end
end
