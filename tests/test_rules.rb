require "./lib/gofish.rb"
require "test/unit"

class TestRules < Test::Unit::TestCase
  include Deck

  def setup
    @game_4 = Game.new(["Player1","Player2","Player3","Player4"])
    @game_4.send(:first_deal)
    @ace_spades     = Card.new("A", "spades")
    @king_clubs     = Card.new("K", "clubs")
    @king_spades    = Card.new("K", "spades")
    @king_hearts    = Card.new("K", "hearts")
    @king_diamonds  = Card.new("K", "diamonds")
  end

  def test_check_current_player_fours
    get_next_player
    @current_player.cards = []
    @current_player.cards = [ @ace_spades,
                              @king_clubs,
                              @king_spades,
                              @king_hearts,
                              @king_diamonds]
    assert_equal(5,@current_player.cards.length)
    @game_4.check_current_player_fours
    assert_equal(1,@current_player.cards.length)
  end

  def test_exit_player
    get_next_player
    @current_player.cards = []
    @game_4.check_exit_player(@current_player)
    assert not(@current_player.active?)
  end

  def test_deal_to_current_player
    get_next_player
    n_of_cards = @current_player.cards.length
    @game_4.deal_to_current_player
    assert_equal(n_of_cards + 1, @current_player.cards.length)
  end

  def test_first_deal
    @game_4.players.each do |p|
      assert_equal(5, p.cards.length)
    end
  end

  def test_next_player()
    get_next_player
    assert_equal("Player1", @current_player.name)
    get_next_player
    assert_equal("Player2", @current_player.name)
    get_next_player
    assert_equal("Player3", @current_player.name)
    get_next_player
    assert_equal("Player4", @current_player.name)
    get_next_player
    assert_equal("Player1", @current_player.name)
    # set Player1 inactive
    @current_player.active = false
    get_next_player
    assert_equal("Player2", @current_player.name)
    get_next_player
    assert_equal("Player3", @current_player.name)
    get_next_player
    assert_equal("Player4", @current_player.name)
    get_next_player
    assert_equal("Player2", @current_player.name)
  end

  def test_game_over
    assert not(@game_4.send(:game_over?))
    get_next_player
    assert @current_player
    until @game_4.deck.empty?
      @game_4.deck.deal(@current_player)
    end
    assert @game_4.deck.empty?
    assert not(@game_4.send(:game_over?))
    @game_4.players.first.cards = []
    assert not(@game_4.send(:game_over?))
    @game_4.players.each do |player|
      player.cards = []
      player.active = false
    end
    assert @game_4.send(:game_over?)
  end

  def test_winners
    get_next_player
    fill_with_fours(["10","J","Q","K","A"])
    assert_equal(20, @current_player.fours.length)
    get_next_player
    fill_with_fours(["7"])
    assert_equal(4, @current_player.fours.length)
    get_next_player
    fill_with_fours(("2".."6"))
    assert_equal(20, @current_player.fours.length)
    get_next_player
    fill_with_fours(("8".."9"))
    assert_equal(8, @current_player.fours.length)
    assert_equal( "Player1,Player3" || "Player3,Player1",
                  @game_4.winners.collect{|p| p.name}.join(',') )
  end

  def test_has_rank
    get_next_player
    @current_player.cards = []
    @current_player.cards.push(@ace_spades)
    assert @game_4.has_rank?("A")
    assert not(@game_4.has_rank?("K"))
  end

  private

    def get_next_player
      @game_4.next_player
      @current_player = @game_4.current_player
    end

    def fill_with_fours(range)
      @current_player.cards = []
      range.each do |r|
        COLORS.each do |c|
          @current_player.cards.push(Card.new(r,c))
        end
        @current_player.check_fours(r)
      end
    end
end
