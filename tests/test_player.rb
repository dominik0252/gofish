require "./lib/gofish.rb"
require "test/unit"

class TestPlayer < Test::Unit::TestCase
  include Deck

  def setup
    @player1        = Player.new("Player1")
    @player2        = Player.new("Player2")
    @player3        = Player.new("Player3")
    @player4        = Player.new("Player4")

    @player1.cards  = [ Card.new("K", "spade"),
                        Card.new("10", "hearts"),
                        Card.new("J", "clubs"),
                        Card.new("J", "diamonds"),
                        Card.new("J", "hearts"),
                        Card.new("J", "spades")]
    @player2.cards  = [ Card.new("K", "hearts"),
                        Card.new("K", "diamonds")]
    @player3.cards  = [ Card.new("9", "hearts"),
                        Card.new("7", "clubs")]
    @player4.cards  = [ Card.new("10", "diamonds"),
                        Card.new("5", "spades")]
  end

  def test_player()
    player = Player.new("Player")

    assert_equal(player.name, "Player")
    assert_equal(player.cards, [])
    assert_equal(player.fours, [])
  end

  def test_ask()
    player1_cards_length = @player1.cards.length
    player2_cards_length = @player2.cards.length
    player3_cards_length = @player3.cards.length
    player4_cards_length = @player4.cards.length
    assert @player1.ask(@player2, "K")
    assert_equal(player1_cards_length + 2, @player1.cards.length)
    assert_equal(player2_cards_length - 2, @player2.cards.length)
    assert @player3.ask(@player4, "9").empty?
    assert_equal(player3_cards_length, @player3.cards.length)
    assert_equal(player4_cards_length, @player4.cards.length)
  end

  def test_fours()
    cards_length = @player1.cards.length
    fours_length = @player1.fours.length
    assert @player1.check_fours("J")
    assert_equal(cards_length - 4, @player1.cards.length)
    assert_equal(fours_length + 4, @player1.fours.length)

    cards_length = @player1.cards.length
    fours_length = @player1.fours.length
    assert not(@player1.check_fours("5"))
    assert_equal(cards_length, @player1.cards.length)
    assert_equal(fours_length, @player1.fours.length)
  end
end
