class Player
  def initialize(name)
    @name   = name
    @cards  = Array.new
    @fours  = Array.new
    @active = true
  end

  attr_reader :name, :fours
  attr_accessor :cards, :active

  def ask(other_player, rank)
    pass_all_rank_cards(other_player.cards, @cards, rank)
  end

  def check_fours(rank = nil)
    ranks_to_check = rank.nil? ? Deck::RANKS : [rank]
    ranks_to_check.each do |r|
      if @cards.select{|card| card.rank == r}.length == 4
        return pass_all_rank_cards(@cards, @fours, r)
      end
    end

    return nil
  end

  def active?
    @active
  end

  private

    def pass_all_rank_cards(from_set, to_set, rank)
      tmp = from_set.dup
      passed_cards = tmp - from_set.delete_if{|card| card.rank == rank}
      to_set.concat(passed_cards)
      return passed_cards
    end
end
