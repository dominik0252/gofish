module Messages
  def print_next_player
    puts
    puts "Next player: #{@current_player.name}"
  end

  def print_asking
    puts  "#{@current_player.name}, say player and card separated by " +
          "space (still playing: #{still_playing.collect{|p| p.name}
                                                .join(', ')}): "
  end

  def print_dealt
    puts
    puts "#{@current_player.name} picked a card."
    print_n_cards_in_deck
  end

  def print_exited(player)
    puts
    puts "#{player.name} exited."
  end

  def print_game_over
    puts
    puts "Game over!"
    puts
    puts "Winners: #{winners.collect{|p| p.name}.join(',')}!"
    puts
    @players.each do |p|
      print_cards(p.fours){ puts "#{p.name}'s fours:" }
    end
  end

  def print_current_player
    puts
    print_cards(@current_player.cards) do
      puts "#{@current_player.name}'s cards:"
    end
    puts
  end

  def print_cards(cards)
    if cards && !cards.empty?
      yield if block_given?
      cards.sort{ |c1, c2| c1.rank <=> c2.rank }.each do |card|
        puts "[#{card.rank}, #{card.color}]"
      end
      puts
    end
  end

  def print_got_cards
    puts "\n###### Got cards ######"
  end

  def print_got_fours
    puts "###### Four of a kind! ######"
  end

  def print_exception(e)
    puts
    puts e.message + " Try again!"
  end

  def print_n_cards_in_deck
    puts "Cards left in deck: #{@deck.cards.length}"
  end
end
