class GameDeck
  include Deck
  # to manipulate GameDeck object as Array
  include Enumerable

  def initialize
    @cards = Array.new
    RANKS.each do |rank|
      COLORS.each do |color|
        card = Card.new(rank, color)
        @cards.push(card)
      end
    end
  end

  # !!! remove
  attr_reader :cards

  def each(&block)
    @cards.each(&block)
  end

  def length
    @cards.length
  end

  def shuffle!
    @cards.shuffle!
  end

  def empty?
    @cards.empty?
  end

  def deal(player)
    player.cards.push(@cards.pop)
  end
end
