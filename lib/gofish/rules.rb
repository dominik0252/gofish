module Rules
  include Messages

  class InvalidInput < StandardError
  end

  def move
    next_player

    if @current_player
      loop do
        check_current_player_fours
        break if @current_player.cards.empty? || fish_cards.empty?
      end

      deal_card_to_current_player
      check_exit_player(@current_player)
    else
      print_game_over if mode == "cl"
      throw(:finished)
    end
  end

  def check_current_player_fours
    fours = @current_player.check_fours
    print_cards(fours) { print_got_fours } if mode == "cl"
  end

  def fish_cards
    asked_player_name, rank = get_player_and_rank
    asked_player =  @players.select{|p| p.name == asked_player_name}
                            .first
    collected_cards = @current_player.ask(asked_player,rank)
    print_cards(collected_cards) { print_got_cards } if mode == "cl"
    check_exit_player(asked_player)
    return collected_cards
  end

  def check_exit_player(player)
    if player.cards.empty?
      player.active = false
      print_exited(player) if mode == "cl"
    end
  end

  def deal_card_to_current_player
    unless @deck.empty?
      deal_to_current_player
      print_dealt if mode == "cl"
    end
  end

  def deal_to_current_player
    @deck.deal(@current_player)
  end

  def first_deal
    @deck.shuffle!
    5.times do
      @players.each do |player|
        @deck.deal(player)
      end
    end
  end

  def next_player
    if !game_over?
      loop do
        @current_player = @players_cycle.next
        break if @current_player.active?
      end
    else
      @current_player = nil
    end
  end

  def get_player_and_rank
    begin
      case mode
      when "cl"
        print_current_player
        print_asking
        asked_player_name, rank = gets.chomp.split(' ')
      # ...
      end

      case
      when !valid_player?(asked_player_name)
        raise InvalidInput, "Non-existent player!"
      when current_player?(asked_player_name)
        raise InvalidInput, "You can't ask yourself!"
      when  !still_playing?(asked_player_name)
        raise InvalidInput, "#{asked_player_name} exited!"
      when !@deck.valid_rank?(rank)
        raise InvalidInput, "Invalid rank!"
      when !has_rank?(rank)
        raise InvalidInput, "You don't have this rank!"
      end

      return [asked_player_name, rank]
    rescue InvalidInput => e
      print_exception(e) if mode == "cl"
      retry
    end
  end

  def still_playing
    @players.select{|p| p.name != @current_player.name && p.active?}
  end

  def still_playing?(player_name)
    still_playing.collect{|p| p.name}.include?(player_name)
  end

  def game_over?
    @deck.empty? && still_playing.empty?
  end

  def winners
    @players.sort!{ |a,b| b.fours.length <=> a.fours.length }
    @players.select{|p| p.fours.length == @players.first.fours.length}
  end

  def has_rank?(r)
    @current_player.cards.collect{|card| card.rank}.include?(r)
  end
end
