class Game
  include Rules

  def initialize(player_names, mode=nil)
    @players = Array.new
    player_names.each do |name|
      @players.push(Player.new(name))
    end
    @players_cycle = @players.cycle
    @deck = GameDeck.new
    @current_player = nil
    @mode = mode
  end

  attr_reader :players, :current_player, :deck, :mode

  def play
    first_deal
    catch(:finished) do
      loop do
        move
      end
    end
  end

  def valid_player?(player_name)
    @players.collect{ |p| p.name }.include?(player_name)
  end

  def current_player?(player_name)
    @current_player.name == player_name
  end
end
