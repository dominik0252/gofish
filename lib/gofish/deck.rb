module Deck
  COLORS = ["clubs","diamonds","hearts","spades"]
  RANKS  = ["A","K","Q","J","10","9","8","7","6","5","4","3","2"]

  class Card
    def initialize(rank, color)
      @rank   = rank
      @color  = color
    end

    attr_reader :rank, :color
  end

  def valid_rank?(r)
    RANKS.include?(r)
  end
end
