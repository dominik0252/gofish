# coding: utf8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name           = "gofish"
  spec.version        = '1.0'
  spec.authors        = ["Dominik Vidusin"]
  spec.email          = ["dominik.vidusin@gmail.com"]
  spec.summary        = %q{Short summary of your project}
  spec.description    = %q{Longer description of your project}
  spec.homepage       = "http://domainforproject.com"
  spec.license        = "MIT"

  spec.files          = ['lib/gofish.rb']
  spec.executables    = ['bin/gofish']
  spec.test_files     = ['tests/test_gofish.rb']
  spec.require_paths  = ["lib"]
end
