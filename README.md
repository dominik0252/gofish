# GoFish game



Author: Dominik Vidusin
Date:   2019-05-24

Ruby version: 2.5.0


A simple command line implementation of the game [Go Fish](https://bicyclecards.com/how-to-play/go-fish/)



Rules for the game can be found on this [link](https://bicyclecards.com/how-to-play/go-fish/)



Start the game by navigating to the root folder and executing

```
$ ruby app.rb
```



Author: [Dominik Vidusin](mailto:dominik.vidusin@gmail.com)
