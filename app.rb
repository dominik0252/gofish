require_relative "lib/gofish"

puts "Enter players separated by space (same names will be ignored):"
players = gets.chomp.split(' ')
game = Game.new(players.uniq, "cl")
game.play
